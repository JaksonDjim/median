package com.djakson;

import java.util.Arrays;

/**
 * Created by Zheka on 07.02.2016.
 */

public class Median {

    public int calcMedian(int[] arr) {
        Arrays.sort(arr);
        int median;
        if (arr.length % 2 == 0)
            median = (arr[arr.length / 2] + arr[arr.length / 2 + 1]) / 2;
        else
            median = arr[arr.length / 2];
        return median;
    }

}