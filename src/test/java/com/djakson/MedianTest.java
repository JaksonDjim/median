package com.djakson;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Zheka on 11.02.2016.
 */
public class MedianTest {

    @Test
    public void testCalcMedian() throws Exception {
        int[] arr = {1, 6, 3, 7, 9, 4, 8};
        int median = new Median().calcMedian(arr);
        assertEquals(6, median);
    }

}